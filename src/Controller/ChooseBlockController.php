<?php

namespace Drupal\layout_builder_experience\Controller;

use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\layout_builder\LayoutBuilderHighlightTrait;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\layout_builder_experience\ExperienceHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ChooseBlockController extends ControllerBase {

  use LayoutBuilderHighlightTrait;
  use AjaxHelperTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The YAML discovery class to find all .layout_options.yml files.
   *
   * @var \Drupal\Core\Discovery\YamlDiscovery
   */
  protected $yamlDiscovery;

  /**
   * The experience helper.
   *
   * @var \Drupal\layout_builder_experience\ExperienceHelper
   */
  protected $experienceHelper;

  /**
   * The block manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * Constructs a ChooseBlockController object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ExperienceHelper $experience_helper) {
    $this->entityTypeManager = $entity_type_manager;
    $this->experienceHelper = $experience_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('layout_builder_experience.helper'),
    );
  }

  /**
   * Insert the add blocks links.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param int $delta
   *   The delta of the section to splice.
   * @param string $region
   *   The region the block is going in.
   *
   * @return array
   *   A render array.
   */
  public function build(SectionStorageInterface $section_storage, $delta, $region) {
    $id = $this->blockAddHighlightId($delta, $region);
    $delta = (int) $delta;
    $result = $this->experienceHelper->getBlocks($section_storage, $delta, $region);

    $blocks = [];

    foreach ($result['inline'] as $item) {
      $link = $this->getBlockLink($section_storage, $delta, $region, $item['id'], $item['label']);
      $block = [
        '#theme' => 'lbe_choose_list_item',
        '#type' => 'block',
        '#label' => $item['label'],
        '#description' => $item['description'],
      ];

      if (!empty($item['icon'])) {
        $block['#icon'] = [
          '#theme' => 'image',
          '#uri' => $item['icon'],
        ];
      }

      $link['#title'] = $block;

      $blocks[] = $link;
    }

    $content = [
      '#theme' => 'lbe_choose_list',
      '#type' => 'block',
      '#items' => $blocks,
      '#see_more' => $this->getSeeMoreLink($section_storage, $delta, $region),
    ];

    return $content;
  }

  protected function getBlockLink(SectionStorageInterface $section_storage, $delta, $region, string $block_id, $title) {
    $url = Url::fromRoute('layout_builder.add_block', [
      'section_storage_type' => $section_storage->getStorageType(),
      'section_storage' => $section_storage->getStorageId(),
      'delta' => $delta,
      'region' => $region,
      'plugin_id' => $block_id,
    ]);

    $attributes = [
      'class' => [
        'lbe-choose-list-link',
        'is-block',
        'js-layout-builder-block-link',
      ],
    ];

    if ($this->isAjax()) {
      $attributes['class'][] = 'use-ajax';
      $attributes['data-dialog-type'] = 'dialog';
      $attributes['data-dialog-renderer'] = 'off_canvas';
    }

    return [
      '#type' => 'link',
      '#url' => $url,
      '#title' => [],
      '#attributes' => $attributes,
    ];
  }

  public function getSeeMoreLink(SectionStorageInterface $section_storage, $delta, $region) {
    $storage_type = $section_storage->getStorageType();
    $storage_id = $section_storage->getStorageId();
    $section = $section_storage->getSection($delta);
    $layout = $section->getLayout();
    $layout_settings = $section->getLayoutSettings();
    $section_label = !empty($layout_settings['label']) ? $layout_settings['label'] : $this->t('Section @section', ['@section' => $delta + 1]);
    $layout_definition = $layout->getPluginDefinition();
    $region_labels = $layout_definition->getRegionLabels();

    return [
      '#type' => 'link',
      '#title' => $this->t('See more blocks <span class="visually-hidden">for @section, @region region</span>', ['@section' => $section_label, '@region' => $region_labels[$region]]),
      '#url' => Url::fromRoute(
        'layout_builder.choose_block',
        [
          'section_storage_type' => $storage_type,
          'section_storage' => $storage_id,
          'delta' => $delta,
          'region' => $region,
        ],
        [
          'attributes' => [
            'class' => [
              'use-ajax',
              'layout-builder__link',
              'lbe-see-more',
            ],
            'data-dialog-type' => 'dialog',
            'data-dialog-renderer' => 'off_canvas',
          ],
        ]
      ),
    ];
  }

}
