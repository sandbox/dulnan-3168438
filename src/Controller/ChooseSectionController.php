<?php

namespace Drupal\layout_builder_experience\Controller;

use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\layout_builder\LayoutBuilderHighlightTrait;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\layout_builder_experience\ExperienceHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a controller to choose a new section.
 *
 * @internal
 *   Controller classes are internal.
 */
class ChooseSectionController implements ContainerInjectionInterface {

  use AjaxHelperTrait;
  use LayoutBuilderHighlightTrait;
  use StringTranslationTrait;

  /**
   * The experience helper.
   *
   * @var \Drupal\layout_builder_experience\ExperienceHelper
   */
  protected $experienceHelper;

  /**
   * The layout manager.
   *
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected $layoutManager;

  /**
   * ChooseSectionController constructor.
   *
   * @param \Drupal\layout_builder_experience\ExperienceHelper $experience_helper
   *   The experience helper.
   */
  public function __construct(ExperienceHelper $experience_helper, $layout_manager) {
    $this->experienceHelper = $experience_helper;
    $this->layoutManager = $layout_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('layout_builder_experience.helper'),
      $container->get('plugin.manager.core.layout')
    );
  }

  /**
   * Choose a layout plugin to add as a section.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param int $delta
   *   The delta of the section to splice.
   *
   * @return array
   *   The render array.
   */
  public function build(SectionStorageInterface $section_storage, $delta) {
    $items = [];
    $layouts = $this->experienceHelper->getLayouts($section_storage, $delta);

    foreach ($layouts as $layout) {
      $instance = $this->layoutManager->createInstance($layout['id']);

      $link = $this->getLink($instance, $layout['id'], $section_storage, $delta);

      $item = [
        '#theme' => 'lbe_choose_list_item',
        '#label' => $layout['label'],
        '#type' => 'section',
        '#description' => $layout['description'],
      ];

      if (!empty($layout['icon'])) {
        $item['#icon'] = [
          '#theme' => 'image',
          '#uri' => $layout['icon'],
        ];
      } else if (!empty($layout['definition_icon'])) {
        $item['#icon'] = $layout['definition_icon'];
      }

      $link['#title'] = $item;

      $items[] = $link;
    }

    return [
      '#theme' => 'lbe_choose_list',
      '#type' => 'section',
      '#items' => $items,
    ];
  }

  private function getLink($layout, $plugin_id, $section_storage, $delta) {
    $route = $layout instanceof PluginFormInterface ? 'layout_builder.configure_section' : 'layout_builder.add_section';
    $url = Url::fromRoute($route, [
      'section_storage_type' => $section_storage->getStorageType(),
      'section_storage' => $section_storage->getStorageId(),
      'delta' => $delta,
      'plugin_id' => $plugin_id,
    ]);

    $attributes = [
      'class' => [
        'lbe-choose-list-link',
        'is-layout',
        'js-layout-builder-section-link',
      ],
    ];

    if ($this->isAjax()) {
      $attributes['class'][] = 'use-ajax';
      $attributes['data-dialog-type'] = 'dialog';
      $attributes['data-dialog-renderer'] = 'off_canvas';
    }

    return [
      '#type' => 'link',
      '#url' => $url,
      '#title' => [],
      '#attributes' => $attributes,
    ];
  }

}
