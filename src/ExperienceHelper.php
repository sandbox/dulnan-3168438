<?php

namespace Drupal\layout_builder_experience;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Discovery\YamlDiscovery;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeExtensionList;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Layout\LayoutDefinition;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Url;
use Drupal\layout_builder\Context\LayoutBuilderContextTrait;
use Drupal\layout_builder\SectionStorageInterface;
use Exception;

/**
 * Class ExperienceHelper.
 */
class ExperienceHelper {

  use LayoutBuilderContextTrait;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The block manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * The layout manager.
   *
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected $layoutManager;

  /**
   * The YAML discovery class to find all .layout_options.yml files.
   *
   * @var \Drupal\Core\Discovery\YamlDiscovery
   */
  protected $yamlDiscovery;

  protected $configBlocks;
  protected $configLayouts;
  protected $blockTypeDescriptions;

  protected $moduleExtensionList;

  protected $themeExtensionListl;

  /**
   * Constructs a new ExperienceHelper object.
   *
   * @param Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Block\BlockManagerInterface $block_manager
   *   The block manager.
   */
  public function __construct(ModuleHandlerInterface $module_handler, ThemeHandlerInterface $theme_handler, BlockManagerInterface $block_manager, LayoutPluginManagerInterface $layout_manager, EntityTypeManagerInterface $entity_type_manager, ModuleExtensionList $module_extension_list, ThemeExtensionList $theme_extension_list) {
    $this->moduleHandler = $module_handler;
    $this->themeHandler = $theme_handler;
    $this->blockManager = $block_manager;
    $this->layoutManager = $layout_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleExtensionList = $module_extension_list;
    $this->themeExtensionList = $theme_extension_list;
  }

  public function getBlocks(SectionStorageInterface $section_storage, $delta, $region) {
    $this->setLayoutOptionsSchema();
    $blocks = $this->getBlockDefinitions($section_storage, $delta, $region);

    $reusable = [];
    $inline = [];

    foreach ($blocks as $id => $definition) {
      $type = $this->getBlockType($id, $definition);
      $icon = $this->getBlockIcon($type);
      $label = $this->getBlockLabel($type, $definition);
      $description = $this->getBlockDescription($type);
      $item = [
        'id' => $id,
        'type' => $type,
        'icon' => $icon,
        'label' => $label,
        'description' => $description,
        'definition' => $definition,
      ];

      if (strpos($id, 'block_content:') === FALSE) {
        $inline[] = $item;
      }
      else {
        $reusable[] = $item;
      }
    }

    $this->sortByLabel($inline);

    return [
      'inline' => $inline,
      'reusable' => $reusable,
    ];
  }

  private function sortByLabel(&$items) {
    if (count($items) > 1) {
      usort($items, function ($item1, $item2) {
        if ($item1['label'] == $item2['label']) return 0;
        return $item1['label'] < $item2['label'] ? -1 : 1;
      });
    }
  }

  private function getBlockType(string $id, array $definition) {
    if (strpos($id, 'block_content:') !== FALSE) {
      // This is what the value looks like:
      // block_content:text:dce763c1-500f-4285-af6f-c25b34a31bca
      if (!empty($definition['config_dependencies']['content'][0])) {
        $parts = explode(':', $definition['config_dependencies']['content'][0]);
        if (!empty($parts[1])) {
          return 'block_content:' . $parts[1];
        }
      }
    }

    return $id;
  }

  private function getBlockIcon(string $type) {
    if (!empty($this->configBlocks[$type]['icon'])) {
      return $this->configBlocks[$type]['icon'];
    }

    return NULL;
  }

  private function getBlockDescription(string $type): string {
    if (!empty($this->configBlocks[$type]['description'])) {
      return $this->configBlocks[$type]['description'];
    }

    if (!empty($this->blockTypeDescriptions[$type])) {
      return $this->blockTypeDescriptions[$type];
    }

    $parts = explode(':', $type);
    if (in_array($parts[0], ['block_content', 'inline_block']) && !empty($parts[1])) {
      /** @var \Drupal\block_content\Entity\BlockContentType $block_type */
      $block_type = $this->entityTypeManager->getStorage('block_content_type')->load($parts[1]);
      if ($block_type) {
        $description = $block_type->getDescription();
        $this->blockTypeDescriptions[$type] = $description;
        return $description;
      }
    }

    return '';
  }

  private function getBlockLabel(string $type, array $definition): string {
    if (!empty($this->configBlocks[$type]['label'])) {
      return $this->configBlocks[$type]['label'];
    }

    if (!empty($definition['admin_label'])) {
      return $definition['admin_label'];
    }

    return '';
  }

  private function getBlockDefinitions(SectionStorageInterface $section_storage, $delta, $region) {
    return $this->blockManager->getFilteredDefinitions('layout_builder', $this->getPopulatedContexts($section_storage), [
      'section_storage' => $section_storage,
      'delta' => $delta,
      'region' => $region,
      'list' => 'inline_blocks',
      'browse' => TRUE,
    ]);
  }

  public function getLayouts(SectionStorageInterface $section_storage, $delta) {
    $this->setLayoutOptionsSchema();
    $items = [];

    $definitions = $this->layoutManager->getFilteredDefinitions('layout_builder', [], ['section_storage' => $section_storage]);

    foreach ($definitions as $plugin_id => $definition) {
      $label = $plugin_id;
      $description = '';
      $icon = '';
      $definition_icon = '';

      if ($definition instanceof LayoutDefinition) {
        $description = $definition->getDescription();
        $label = $definition->getLabel();
        $definition_icon = $definition->getIcon(142, 80, 1, 3);
      }

      if (!empty($this->configLayouts[$plugin_id]['description'])) {
        $description = $this->configLayouts[$plugin_id]['description'];
      }

      if (!empty($this->configLayouts[$plugin_id]['label'])) {
        $label = $this->configLayouts[$plugin_id]['label'];
      }

      if (!empty($this->configLayouts[$plugin_id]['icon'])) {
        $icon = $this->configLayouts[$plugin_id]['icon'];
      }

      $items[] = [
        'id' => $plugin_id,
        'label' => $label,
        'description' => $description,
        'icon' => $icon,
        'definition_icon' => $definition_icon,
        'definition' => $definition,
      ];
    }

    $this->sortByLabel($items);

    return $items;
  }

  private function getLayoutDefinitions(SectionStorageInterface $section_storage, $delta, $region) {
    return $this->layoutManager->getFilteredDefinitions('layout_builder', $this->getPopulatedContexts($section_storage), [
      'section_storage' => $section_storage,
    ]);
  }

  /**
   * Gets the layout options scheme defined in the layout_options.yml files.
   *
   * This is a merge of all the yaml files with the last loaded taking
   * precidence. The order is based on Drupal's module load order followed by
   * the theme load
   * order.
   *
   * @return string[]
   *   The layout options scheme or an empty array if no files found.
   */
  public function setLayoutOptionsSchema() {
    if (isset($this->layoutOptionsSchema)) {
      return;
    }

    try {
      $results = $this->getYamlDiscovery()->findAll();
    }
    catch (Exception $e) {
      return [];
    }

    $layoutOptionsSchema = [];
    foreach ($results as $module => $config) {
      $resolved = $this->resolvePaths($module, $config);
      $layoutOptionsSchema = NestedArray::mergeDeep($layoutOptionsSchema, $resolved);
    }

    $this->layoutOptionsSchema = $layoutOptionsSchema;

    $this->configBlocks = $layoutOptionsSchema['blocks'] ?? [];
    $this->configLayouts = $layoutOptionsSchema['layouts'] ?? [];
  }

  public function resolvePaths($module, $module_config) {
    $resolved = [];
    $path = $this->getItemPath($module);

    if (!$path) {
      return [];
    }

    foreach ($module_config as $type => $config) {
      foreach ($config as $item => $item_config) {
        $resolved[$type][$item] = $item_config;
        if (!empty($item_config['icon'])) {
          $resolved[$type][$item]['icon'] = '/' . $path . '/' . $item_config['icon'];
        }
      }
    }

    return $resolved;
  }

  public function getItemPath($item) {

    if ($path = $this->moduleExtensionList->getPath($item)) {
      return $path;
    }

    if ($path = $this->themeExtensionList->getPath($item)) {
      return $path;
    }

    return NULL;
  }

  /**
   * Gets the YAML discovery object used to load the layout_options yaml files..
   *
   * @return \Drupal\Core\Discovery\YamlDiscovery
   *   The YAML discovery object.
   */
  public function getYamlDiscovery() {
    if (!isset($this->yamlDiscovery)) {
      $this->yamlDiscovery = new YamlDiscovery(
        'lb_experience',
        $this->moduleHandler->getModuleDirectories() +
        $this->themeHandler->getThemeDirectories()
      );
    }
    return $this->yamlDiscovery;
  }
}